<?php

/**
 * @file
 * Module that turns Drupal pager URLs into SEO-frendlier ones, like
 * nodealias.html?page=0,2 to nodealias/page2.html.
 * 
 * Now only one naming convention is supported:
 * - first page: <nodealias>.html
 * - second page: <nodealias>/page2.html
 * - Nth page: <nodealias>/pageN.html
 *
 */

// have it run on bootstrap for url_inbound_alter to work
function pager_cleaner_boot() {
}

function pager_cleaner_url_inbound_alter(&$result, $path, $path_language) {
  $page = 0;
  $resultingUrl = '';
  if (_pager_cleaner_unalias_page_url($path, $resultingUrl, $page)) {
    $result = drupal_get_normal_path($resultingUrl);
    $_GET['q'] = $result;

    // @todo We imply only 1 pager on a page. This might become a problem. for someone. in distant future.
    //$pageVar = isset($_GET['page']) ? $_GET['page'] : '0,0';
    //$page_elements = explode(',', $pageVar);

    $_GET['page'] = implode(',', array(0, $page-1)); //$pageVar[1]
  }
}

function pager_cleaner_url_outbound_alter(&$path, &$options, $original_path) {
  $query = array();
  if (is_string($options['query'])) {
    parse_str(urldecode($options['query']), $query);
  } else if (is_array($options['query'])) {
    $query = $options['query'];
  } else {
    return;
  }
  if (!isset($query['page'])) {
    return;
  }

  $page = explode(',', $query['page']);
  $proposedPath = _pager_cleaner_get_aliased_page_url($path, $page[1]+1);

  if ($proposedPath != $path) {
    //dpm("altering out-url: $path to $proposed_path");
    unset($query['page']);
    $options['query'] = $query;
    // @todo Check if $page has enough elements
    $path = $proposedPath;
  }
}


/**
 * 
 * @param $nid node id to add page suffix
 * @param $page 1-based page number
 * @return altered url
 */
function _pager_cleaner_get_aliased_node_url($nid, $page) {
  $pageAlias = drupal_get_alias("node/$nid");
  return _pager_cleaner_get_aliased_page_url($pageAlias, $page+1);
}  

/**
 * 
 * @param $url URL to add page suffix
 * @param $page 1-based page number
 * @return altered url
 */
function _pager_cleaner_get_aliased_page_url($url, $page) {
  if ($page == 1) {
    return $url;
  }

  if (preg_match('|(.+)\.html|', $url, $matches)) {
    $path = "${matches[1]}/page$page.html";
    return $path;
  }

  // @todo how to better report non-conforming page URL?
  //dpm("Warning: can't alias an URL ($url)/page{$page}");
  return $url;
}  

/**
 * 
 * @param $url - URL to parse
 * @param $originalUrl - out parameter
 * @param $page 1-based page number
 * @return true if the url was altered by paging convention
 */
function _pager_cleaner_unalias_page_url($url, &$originalUrl, &$page) {
  if (preg_match('|(.*)/page(\d+)\.html|', $url, $matches)) {
    $originalUrl = $matches[1] . '.html';
    $page = $matches[2];
    return true;
  } else {
    $originalUrl = $url;
    $page = 0;
    return false;
  }
}


function pager_cleaner_help($section) {
  switch ($section) {
    case 'admin/help#pager_cleaner':
      // Return a line-break version of the module README.txt
      return filter_filter('process', 1, NULL, file_get_contents( dirname(__FILE__) . "/README.txt") );
  }
}
