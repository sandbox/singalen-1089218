Pager cleaner

Module that turns Drupal pager URLs into SEO-frendlier ones, like
nodealias.html?page=0,2 to nodealias/page2.html.

Now only one naming convention is supported:
- first page: <nodealias>.html
- second page: <nodealias>/page2.html
- Nth page: <nodealias>/pageN.html

Installing

1. Set up your pages' aliases to end with .html 
(some day we'll support other convections)

2. Enable the module, and your links turn from 
"about.html?page=0,2" to "about/page2.html"